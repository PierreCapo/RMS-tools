#Place this file in the map folder and run it to convert your maps from AOM to AOE3
#If you want to make the contrary (AOE3 to AOM) you need to exchange the entries in the "replacements" diccionary

from os import rename, listdir, path
import fileinput
import re

if __name__=="__main__":
    fnames = listdir('.')

    for fname in fnames:
        if fname.endswith(".xml"):
            tempfile=open(fname,'r+')
            tempfile.write('<?xml version = \"1.0\" encoding = \"UTF-8\"?> \n' +
                           '<mapinfo detailsText=\"Plop\" \n'
                            +'imagepath  = \"ui\\random_map\\new_england\" \n'
                            +'displayName = \"'+path.splitext(fname)[0]+'\" \n'
                            +'cannotReplace = \"\" \n'
                            +'loadDetailsText = \"Plop\" \n'
                            +'loadBackground=\"ui\\random_map\\new_england\\new_england_map\" \n'
                            +'> \n'

                            +'<loadss>ui\\random_map\\great_lakes\\great_lakes_ss_03</loadss> \n'
      
                            +'</mapinfo>')
            tempfile.close()

        
    replacements = {
        '\"Settlement Level 1\"':'\"TownCenter\"', '\"Tower\"':'\"Outpost\"',
        '\"Gold mine small\"':'\"mine\"', '\"Gold mine\"':'\"mine\"',
        '\"tree snow pine\"':'\"TreeYukonSnow\"', '\"pine snow\"':'\"TreeYukon\"','\"palm\"':'\"TreeCaribbean\"', '\"palm tree\"':'\"ypTreeBorneoPalm\"',
        '\"elephant\"':'\"ypWildElephant\"', '\"gazelle\"':'\"Deer\"', '\"elephant\"':'\"ypWildElephant\"', '\"Aurochs\"':'\"muskOx\"',
        
        '\"Oak Forest\"':'\"Great Plains Forest\"', '\"Palm Forest\"':'\"Caribbean Palm Forest\"', '\"Pine Forest\"':'\"Christams Forest\"', '\"savannah forest\"':'\"Borneo Palm Forest\"',
        '\"Caribbean Palm Forest\"':'\"Amazon Rain Forest\"','\"AUTUMN OAK FOREST\"':'\"Great Plains Forest\"', '\"HADES FOREST\"':'\"Caribbean Palm Forest\"', '\"MARSH FOREST\"':'\"Christams Forest\"',
        '\"MIXED OAK FOREST\"':'\"Great Plains Forest\"', '\"MIXED PALM FOREST\"':'\"Caribbean Palm Forest\"', '\"MIXED PINE FOREST\"':'\"Christams Forest\"', '\"SNOW PINE FOREST\"':'\"Christams Forest\"',
        '\"TUNDRA FOREST\"':'\"Great Plains Forest\"',
        
        '\"mediterranean sea\"':'\"Seville\"', '\"Greek River\"':'\"Great Lakes\"', '\"Egyptian Nile\"':'\"Amazon River Basin\"',
        '\"AEGEAN SEA\"':'\"Seville\"', '\"EGYPTIAN NILESHALLOW\"':'\"Great Lakes\"', '\"MARSHPOOL\"':'\"Amazon River Basin\"',
        '\"NORSE RIVER\"':'\"Seville\"', '\"NORTH ATLANTIC OCEAN\"':'\"Great Lakes\"', '\"NORWEGIANSEA\"':'\"Amazon River Basin\"',
        '\"RED SEA\"':'\"Seville\"', '\"SAVANNAH WATER HOLE\"':'\"Great Lakes\"', '\"STYX RIVER\"':'\"Amazon River Basin\"',
        '\"TUNDRA\"':'\"Seville\"', '\"TUNDRA POOL\"':'\"Great Lakes\"', '\"WATERING HOLE\"':'\"Amazon River Basin\"',
        
        '\"SandA\"':'\"sonora\\\\ground1_son\"', '\"SandB\"':'\"sonora\\\\ground6_son\"', '\"SandC\"':'\"california\\\\desert2_cal\"', '\"SandD\"':'\"borneo\\\\shoreline1_bor\"',
        '\"SnowA\"':'\"yukon\\\\ground4_yuk\"', '\"SnowB\"':'\"rockies\\\\groundsnow2_roc\"', '\"SnowC\"':'\"yukon\\\\ground5_yuk\"', '\"SnowD\"':'\"yukon\\\\ground3x_yuk\"',
        '\"GrassA\"':'\"california\\\\ground5_cal\"', '\"GrassB\"':'\"california\\\\ground6_cal\"', '\"GrassC\"':'\"california\\\\ground8_cal\"', '\"GrassD\"':'\"california\\\\groundforest_cal\"'}
        
    fnames = listdir('.')
    for fname in fnames:
        lines = []
        linesBug =[]
        if fname.endswith(".xs"):
            
            with open(fname) as infile:
                for line in infile:
                    if not 'rmSetGaiaCiv' in line: #this command doesn't exist on AOE3 so it makes the map crashing
                        for src, target in replacements.items():
                            src_str= re.compile(src, re.IGNORECASE)
                            line = src_str.sub(target,line)
                        lines.append(line)
            with open(fname, 'w') as outfile:
                for line in lines:
                    outfile.write(line)
