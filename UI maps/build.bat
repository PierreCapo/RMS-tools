del "ESOC_Maps_UI_AutoInstaller.exe"
cd Resources
python "Inno Generator.py"

:: Compile our installer
"..\Inno Setup\Compil32.exe" /cc ".\Installer Builder.iss"

move "Output\ESOC_Maps_UI_AutoInstaller.exe" "..\ESOC_Maps_UI_AutoInstaller.exe"

rmdir "Output" /s /q

cd ..