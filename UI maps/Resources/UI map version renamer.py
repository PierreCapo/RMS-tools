from os import rename, listdir
import fileinput

old_sufix = "UI(2,2)"
new_sufix = "UI(2,2a)"
old_xml = "2.2 "
new_xml = "2.2a "
fnames = listdir('..\\Maps\\')

for fname in fnames:
    if fname.endswith(old_sufix + ".xs"):
        rename(fname,fname.replace(old_sufix, new_sufix, 1))
    if fname.endswith(old_sufix + ".xml"):
        rename(fname,fname.replace(old_sufix, new_sufix, 1))

    
replacements = {'2.2 ':'2.2a '}
fnames = listdir('..\\Maps\\')
for fname in fnames:
    lines = []
    with open(fname) as infile:
        for line in infile:
            for src, target in replacements.items():
                line = line.replace(src, target)
            lines.append(line)
    with open(fname, 'w') as outfile:
        for line in lines:
            outfile.write(line)
