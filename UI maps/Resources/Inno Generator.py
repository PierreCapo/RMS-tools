from os import walk

#Get all map file names
m = []
for (dirpath, dirnames, filenames) in walk("..\Maps"):
    m.extend(filenames)

# Get the last line called [File]
f = open(".\Installer Builder.iss")
lines = f.readlines()
counter=0
last_line=0
for line in lines:
    counter+=1
    if "[Files]" in line:
        last_line=counter
f.close()

# Delete lines after it and add the new ones
w = open("Installer Builder.iss","w")
w.writelines([item for item in lines[:last_line]])
for element in m:
    w.writelines('Source: "..\\Maps\\' + element + '"; DestDir: "{userdocs}\\My Games\\Age of Empires 3\\RM3"; Flags: ignoreversion \n')
    w.writelines('Source: "..\\Maps\\' + element + '"; DestDir: "{userdocs}\\My Games\\Age of Empires 3\\RMF"; Flags: ignoreversion \n')
w.close()

