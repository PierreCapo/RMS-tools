**Set up the environment**

-Download Python 3.x

**UI Maps**

_1._ Place all the maps that you want to be add in the installer in the "Maps" folder.
_2._ Edit the Version renamer if you want to have different version name from "2.2". You need to place this python file in the Map folder, and then run it to make it work.
_3._ Run the build.bat file
_4._ An .exe has been generated in the main folder if everything has runned correctly. You can rename it if you don't want to have ESOC name

**AOM maps to TAD maps**

_1._ Place the python script in the AOM maps folder. Run the python script. The maps have been converted to TAD !
